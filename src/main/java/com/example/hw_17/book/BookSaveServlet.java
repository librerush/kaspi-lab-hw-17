package com.example.hw_17.book;

import com.example.hw_17.HibernateUtil;
import jakarta.servlet.*;
import jakarta.servlet.http.*;
import jakarta.servlet.annotation.*;
import org.hibernate.Session;
import org.hibernate.Transaction;

import java.io.IOException;
import java.util.List;

@WebServlet(name = "BookSaveServlet", value = "/book-save")
public class BookSaveServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        getAllBooks(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Book book = new Book();
        book.setName(request.getParameter("name"));
        book.setAuthor(request.getParameter("author"));
        book.setPublisher(request.getParameter("publisher"));
        book.setYear(Long.parseLong(request.getParameter("year")));

        Session session = HibernateUtil.getSessionFactory().openSession();
        Transaction transaction = session.beginTransaction();

        Long id = (Long) session.save(book);

        System.out.printf("(%d) book is saved\n", id);

        transaction.commit();
        session.close();

        getAllBooks(request, response);
    }

    private void getAllBooks(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Session session = HibernateUtil.getSessionFactory().openSession();
        List<Book> bookList = session.createQuery("from Book ", Book.class).list();
        request.setAttribute("books", bookList);
        session.close();
        getServletContext().getRequestDispatcher("/book/list-book.jsp").forward(request, response);
    }

    @Override
    public void destroy() {
        HibernateUtil.getSessionFactory().close();
    }
}
