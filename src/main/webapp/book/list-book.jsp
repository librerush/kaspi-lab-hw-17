<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>
<head>
    <title>List of books</title>
</head>
<body>
<div class="container">
    <div class="row">

        <h1>List of all books</h1>

        <table class="table">
            <tr>
                <td>ID</td>
                <td>Name</td>
                <td>Author</td>
                <td>Publisher</td>
                <td>Year</td>
            </tr>

            <c:forEach items="${books}" var="b">
                <tr>
                    <td>${b.id}</td>
                    <td>${b.name}</td>
                    <td>${b.author}</td>
                    <td>${b.publisher}</td>
                    <td>${b.year}</td>
                </tr>
            </c:forEach>

        </table>


        <a href="book/add_book.jsp">Add a new book</a>
    </div>
</div>
</body>
</html>
