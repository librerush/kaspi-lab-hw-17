<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add a book</title>
</head>
<body>
<form method="post" action="../book-save">
    <div>
        Name
        <input type="text" name="name">
        <b></b><b></b>
    </div>
    <div>
        Author
        <input type="text" name="author">
        <b></b><b></b>
    </div>
    <div>
        Publisher
        <input type="text" name="publisher">
        <b></b><b></b>
    </div>
    <div>
        Year
        <input type="text" name="year">
    </div>
    <b></b>
    <button type="submit">Add</button>
</form>
</body>
</html>
